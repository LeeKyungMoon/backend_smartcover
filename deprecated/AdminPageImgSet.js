var express = require("express");
var app = express();
app.use(express.bodyParser({
	keepExtensions: true,
	uploadDir: "uploads",
	limit: 10000000
}));
app.post("/upload", function (request, response) {
    console.log("filename: ", request.files.file.name);
    var temppath = request.files.file.path;
    temppath=temppath.replace(/\\/g,'/');
    console.log("filepath: ", temppath);
    
    response.end('<!DOCTYPE html><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><html><body>업로드 완료. 다음을 복사하십시오: <input type="text" size="50" value="http://172.16.100.172/'+temppath+'"></input></body></html>');
});

app.get("/", function (request, response) {
});

app.listen(52275);